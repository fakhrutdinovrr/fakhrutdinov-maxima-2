import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 21.12.2021
 * 21. Collection Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Bank {

    private Map<User, List<Transaction>> transactions;

    public Bank() {
        this.transactions = new HashMap<>();
    }

    // метод для перевода денег
    public void sendMoney(User from, User to, int sum) {
        // создаем сам перевод
        Transaction transaction = new Transaction(from, to, sum);
        // проверить, если ли уже у нас история переводов этого пользователя или нет
        // если еще переводов не было
        if (!transactions.containsKey(from)) {
            // кладу пользователю пустой список
            transactions.put(from, new ArrayList<>());
        }
        // get - получает список всех транзакций пользователя from и добавляет транзакцию в этот список
        transactions.get(from).add(transaction);
    }

    public List<Transaction> getTransactionsByUser(User user) {
        return transactions.get(user);
    }

    // вернуть сумму всех транзакций по конкретному пользователю
    public int getTransactionsSum(User user) {
        if (transactions.containsKey(user)) {
            int sum = 0;
            for (int i = 0; i < transactions.get(user).size(); i++) {
                sum += transactions.get(user).get(i).getSum();
            }
            return sum;
        }
        return -1;
    }

    public void showNamesAndTransactions() {
        for (Map.Entry<User,List <Transaction>> entry : this.transactions.entrySet()) {
            String name = entry.getKey().getName();
            int sum = entry.getValue().size();
            System.out.println(name +" "+ sum);
        }
        
    }

}
