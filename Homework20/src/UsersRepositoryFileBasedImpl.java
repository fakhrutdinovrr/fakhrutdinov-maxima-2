import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 29.01.2022
 * 25. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFileBasedImpl {

    private String fileName;

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    // возвращает список всех пользователей из файла
    public List<User> findAll() {
        List users = new ArrayList();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(this.fileName));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        try {
            String text = reader.readLine();
            while (text != null) {
                String[] name = text.split("\\|");
                users.add(new User(name[0], name[1]));
                text = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // выполняется всегда, независимо от ошибок
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    public Optional<User> findByFirstName(String firstName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String text = reader.readLine();
            while (text != null) {
                String[] array = text.split("\\|");
                if (array[0].equals(firstName)) {
                    User user = new User(array[0], array[1]);
                    return Optional.of(user);
                }
                text = reader.readLine();
            }
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void save(User user) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(user.getFirstName() + "|" + user.getLastName());
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // выполняется всегда, независимо от ошибок
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}
