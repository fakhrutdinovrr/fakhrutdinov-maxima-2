import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.print("Enter array size: ");

        int arraySize = scanner.nextInt();
        int[] array = new int[arraySize];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000);
        }

        System.out.print("Enter thread count: ");
        int threadsCount = scanner.nextInt();

        int StepsCount = div(array.length,threadsCount);

        for (int i = 0; i < threadsCount; i++) {
            int from = StepsCount * i;
            int to = from + StepsCount;
            if (to > array.length) {
                to = array.length;
            }
            Thread counter = new Counter(from, to, array);
            counter.start();
            counter.join();
        }

        int sum = 0;

        for (int number : array) {
            sum += number;
        }

        System.out.println(sum);
    }

    public static int div(int arraySize, int threadCount) {
        double temp = (double) arraySize / threadCount;
        if (temp % 1 == 0) {
            return (int) temp;
        } else {
            return (int) temp + 1;
        }
    }

}
