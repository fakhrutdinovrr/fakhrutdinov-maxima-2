## 20 минут

# Задание 01 - Intro

* [Установить JDK](https://www.oracle.com/java/technologies/downloads/)

* [Sublime Text](https://www.sublimetext.com/3)

* Повторить то, что я делал в видео, чтобы у вас работала программа, как у меня

## Задание 02 - Variables

```java
import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int number = scanner.nextInt();

		// ...

		// необходимо вывести двоичное представление числа number
	}
}
```

* Гарантируется, что `0 <= number <= 127`

* Нельзя использовать массивы, циклы, строки, Integer. 

## Задание 03 - Sequence

```
На вход подается последовательность чисел:

a0, a1, a2, a3, ... aN

При этом N -> infinity, aN = -1

Program1 - необходимо найти число, у которого сумма цифр минимальная среди всех остальных.

34 - 3 + 4 = 7
1112 - 1 + 1 + 1 + 2 = 5
2321 - 2 + 3 + 2 + 1 = 8
12321 - 1 + 2 + 3 + 2 + 1 = 9

ОТВЕТ: 1112

Program2 - необходимо найти количество локальных минимумов

Локальный минимум, если выполняется условие - ai-1 > ai < ai+1

34
20
11 -> локальный минимум, потому что 20 > 11 < 15
15
17
8 -> локальный минимум, потому что 17 > 8 < 21
21

```

## Задание 04 - Array

```
Объявить массив (размер - 10). Заполнить его элементами из консоли.
Вывести в обратном порядке.
```

## Задание 05 - ArrayExtended

```
На вход подается список возрастов людей (мин 0 лет, макс 120 лет)
Необходимо вывести информацию о том, какой возраст чаще всего встречается.

45
30
30
45
45
30
30
12
-1

Вывод - 30 лет
```

## Задание 06 - ArraySortAndSearch

```
Реализовать в Program процедуру void selectionSort(int[] array) и функцию boolean search(int[] array) с бинарным поиском.
```

## Задание 07 - Integral

```
Реализовать вычисления интеграла методом Симпсона (формулу и пример реализации смотрите в конспекте занятия).
```

## Задание 08 - Recursion

```
Написать рекурсивную функцию int sumOfDigits(int number) - возвращает сумму цифр числа.
```

## Задание 09 - Bus

```
На базе проекта 12.Bus добавить функциональность:

У водителя сделать метод `drive`, который заставит "поехать" автобус. 
Когда автобус едет, все его пассажири называют свои имена.
Когда автобус едет, нельзя менять водителя, нельзя, чтобы пассажиры покидали свои места, 
нельзя, чтобы автобус принимал пассажиров.
```

## Задание 10 - Logger

```
Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.

Применить паттерн Singleton для Logger.
```

## Задание 11 - ArrayList & LinkedList

```
Реализовать методы TODO
В Main проверить работу всех методов
```

## Задание 12 - Figures

```
Реализовать классы Circle и Ellipse (очевидно, что Circle - потомок Ellipse)
Необходимо, чтобы в свою очередь Ellipse и Rectangle были потомками Figure
У Figure необходимо описать два поля - x и y (координаты центра фигуры)
Реализовать метод move, который переносит фигуры в новые координаты
В каждом классе необхдоимо переопределить метод getPerimeter.

В main создать экземпляр каждого класса и показать, что они работают (вызвать периметр у каждой фигуры).
Кажду фигуру перенести в точку (66, 77)
```

## Задание 13 - 3D Figures

```
В моем example5 есть некоторые проблемы:

- метод move везде реализован одинаково (решить)
```

## Задание 14 - GenericArrayList

```
до 11-го числа.

Дореализовать обощенный ArrayList, LinkedList по аналогии с моей реализацией
```

## Задание 15 - HashMapImpl

```
Попробуйте реализовать Мап на основе хешей строк. 
Хеш использовать в качестве индекса массива.
Реализовать только put
Не забывайте про % (взятие остатка)
```

## Задание 16 - MapAndSet

```
Используя Set и Map, которые мы реализовали
для входной строки посчитать, сколько раз встречается каждое слово из входного текста

ВХОД:
привет привет марсель в казани ты тоже в казани

ОТВЕТ:
Привет - 2 раза
марель - 1 раз
ты - 1 раз
тоже - 1 раз
в - 2 раза
казани - 2 раза

String text = scanner.nextLine();
String words[] = text.split(" "); // Разбить текст на слова можно 
```

## Задание 17

```
Реализовать TODO методы в Task2

Использовать только встроенные коллекции - List, ArrayList, HashMap

* Вывести все имена пользователей и количество их транзакций
```

## На каникулы до 11.01.2021

```
1. Повторить все темы
2. Доделать все дз
3. Если будут вопросы на каникулах, мне задавайте, но не факт что тут же отвечу (c 4-го)
4. Если нужны консультации - пишите (с 4-го)    
5. ДЗ конкретное будет 30.12.2021
```

## Задание 18

```
Считать из файла текст (ТОЛЬКО НА АНГЛИЙСКОМ), выяснить, какие слова встречаются чаще других и вывести их на экран.
```

## Задание 19

```
Реализовать класс TextConverter, в котором будет метод void toLowerCaseAll(String sourceFileName, String targetFileName).

Данный метод принимает на вход два имени файла. 
В первом (исходном) файле дан текст на английском языке (и еще могут быть символы, но все гарантированно не Unicode).
Метод считывает весь текст, все большие буквы делает маленькими и убирает все "небуквенные символы", кроме пробелов.
Весь этот текст записывает в целевой файл с названием targetFileName

Hello! How are you?

Результат:

hello how are you

Можно использовать 

Character.isLetter();
Character.toLowerCase();
```

## Задание 20

```
Реализовать в UsersRepository findAll() и findByFirstName()
```

## Задание 21

```
Реализовать проверку, а не нарушена ли уникальность email-а при сохранении пользователя?

usersService.signUp("sidikov@marsel.com", "qwerty007");
usersService.signUp("sidikov@marsel.com", "qwerty008");
Должна быть выброшена ошибка UnuniqueEmailException
```

## Задание 22

```
Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

o001aa111|Camry|Black|133|82000
o002aa111|Camry|Green|133|0
o001aa111|Camry|Black|133|82000

Используя Java Stream API, вывести:

* Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map - решено на занятии
* Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
* Вывести цвет автомобиля с минимальной стоимостью. // min + map
* Среднюю стоимость Camry

https://habr.com/ru/company/luxoft/blog/270383/
```

## Задание 23

```
Создать таблицу "ПРОДОВОЛЬСТВЕННЫЕ ТОВАРЫ"

Колонки:
- id
- название товара
- стоимость 
- срок годности
- дата поступления товара
- название поставщика

Запросы:
- Получить товары, которые дороже 100 рублей
- Получить товары, которые были поставлены ранее 02-02-2020 года
- Получить названия всех поставщиков *

Готовый sql-файл залить в git

Шаги:
- установить ultimate
- установить PostgreSQL
- создать базу
- подключиться из IDEA
- написать все запросы
- залить в git
```

## Задание 24

```
1. Регистрируемся на сайте https://www.sql-ex.ru/?Lang=0
2. Решаем упражнения пока есть силы :)
3. В чате друг другу помогаем
```

## Задание 25

```
Вывести информацию из таблицы в задании 23
```

## Задание 26

```
Сделать приложение с одним ProductsRepository/ProductsRepositoryJdbcImpl

Предусмотреть методы:

List<Product> findAllByPrice(double price); // возвращает все товары одной стоимости
Optional<Product> findOneByName(String name); // найти товар по названию
void update(Product product); // обноваить товар
void delete(Product product); // удалить из базы данных

В Main проверить работоспособность
```

## Задание 27

```
То же самое, что и в задании 26, только на JdbcTemplate
```

## Задание 28

```
У вас есть массив длины N, также у вас есть количество потоков K.

Необходимо, чтобы было запущено K-потоков, и каждый из них посчитал свою сумму чисел промежутка массива.

Например:

array - 3, 6, 7, 1, 3, 5, 6, 4, 19, 11
K = 3

Тогда:
первый поток - 3 + 6 + 7 + 1
второй поток - 3 + 5 + 6 + 4
третий поток - 19 + 11

Разбиение может быть другим.
```

## Задание 29

```
Есть проект 29. Console Application

Ваша задача - точно по видео повторить все манипулции с кодом (интеграция Spring).
```

## Задание 30

```
Пройти HTML Academy

Написать свой Яндекс
```

## Задание 31

* Написать REST API для товаров.
* Отдельная операция - сделать товар недоступным для заказа сейчас.