public class Main {
    public static int sumfOfDigit(int number) {
        if (number < 10) {
            return number;
        }
        int digitSum = number % 10;
        digitSum += sumfOfDigit(number / 10) ;
        return digitSum;
    }

    public static void main(String[] args) {
        System.out.println(sumfOfDigit(12345));
    }
}
