package repositories;

import Models.Product;
import Util.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ProductRepositoryJdbcImpl implements ProductRepository {

    DataSource dataSource;
    //language=SQL
    private static final String SQL_DELETE_FROM_FOODSTUFFS_BY_ID = "delete from foodstuffs " +
            " where id = ?";
    //language=SQL
    private static final String SQL_INSERT_INTO_FOODSTUFFS = "insert into foodstuffs(product_name,price,expiration_date,date_of_receipt,supplier_name) " +
            "values (?,?,?,?,?)";
    //language=SQL
    private static final String SQL_SELECT_FIRST_BY_PRODUCT = "select * from foodstuffs " +
            "where product_name = ? limit 1";
    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from foodstuffs " +
            "where price = ?";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from foodstuffs " +
            "where id = ? limit 1";
    //language=SQL
    private static final String SQL_UPDATE_FOODSTUFFS_BY_ID = "update foodstuffs set product_name = ?, price = ?, expiration_date = ?, date_of_receipt = ?, supplier_name = ? " +
            "where id = ?";

    private static final RowMapper<Product> productRowMapper = row -> new Product(
            row.getLong("id"),
            row.getString("product_name"),
            row.getDouble("price"),
            row.getString("expiration_date"),
            row.getString("date_of_receipt"),
            row.getString("supplier_name"));


    public ProductRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Product product) {

        Date data_expiration = null;
        Date date_receipt = null;

        try {
            Date date = new SimpleDateFormat("YYYY-MM-DD").parse(product.getExpiration_date());
            data_expiration = new java.sql.Date(date.getTime());
            Date date1 = new SimpleDateFormat("YYYY-MM-DD").parse(product.getDate_of_receipt());
            date_receipt = new java.sql.Date(date1.getTime());


        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_FOODSTUFFS, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, product.getProduct_name());
            statement.setDouble(2, product.getPrice());
            statement.setDate(3, (java.sql.Date) data_expiration);
            statement.setDate(4, (java.sql.Date) date_receipt);
            statement.setString(5, product.getSupplier_name());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert Product");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();

            // если сгенерированных ключей нет
            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated id");
            }

            Long generatedId = generatedKeys.getLong("id");

            product.setId(generatedId);


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        List<Product> products = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_PRICE)) {
            statement.setDouble(1, price);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Product product = productRowMapper.mapRow(resultSet);
                    products.add(product);
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return products;
    }

    @Override
    public Optional<Product> findOneByName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_FIRST_BY_PRODUCT)) {
            statement.setString(1, name);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Product product = productRowMapper.mapRow(resultSet);
                    return Optional.of(product);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {

        Date data_expiration = null;
        Date date_receipt = null;

        try {
            Date date = new SimpleDateFormat("YYYY-MM-DD").parse(product.getExpiration_date());
            data_expiration = new java.sql.Date(date.getTime());
            Date date1 = new SimpleDateFormat("YYYY-MM-DD").parse(product.getDate_of_receipt());
            date_receipt = new java.sql.Date(date1.getTime());

        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_FOODSTUFFS_BY_ID)) {

            statement.setString(1, product.getProduct_name());
            statement.setDouble(2, product.getPrice());
            statement.setDate(3, (java.sql.Date) data_expiration);
            statement.setDate(4, (java.sql.Date) date_receipt);
            statement.setString(5, product.getSupplier_name());
            statement.setLong(6, product.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update Product");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Product> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    Product product = productRowMapper.mapRow(resultSet);

                    if (resultSet.next()) {
                        throw new SQLException("more one rows");
                    }

                    return Optional.of(product);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_FROM_FOODSTUFFS_BY_ID)) {

            statement.setLong(1, product.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert user");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }
}
