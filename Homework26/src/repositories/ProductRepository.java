package repositories;

import Models.Product;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    public void save(Product product);

    List<Product> findAllByPrice(double price);

    Optional<Product> findOneByName(String name);

    void update (Product product);

    Optional<Product> findById(Long id);

    void delete(Product product);

}
