package Models;

import java.util.Objects;

public class Product {
    private Long id;
    private String product_name;
    private double price;
    private String expiration_date;
    private String date_of_receipt;
    private String supplier_name;

    public Product(Long id, String product_name, double price, String expiration_date, String date_of_receipt, String supplier_name) {
        this.id = id;
        this.product_name = product_name;
        this.price = price;
        this.expiration_date = expiration_date;
        this.date_of_receipt = date_of_receipt;
        this.supplier_name = supplier_name;
    }

    public Product(Long id, String product_name, String expiration_date, String date_of_receipt, String supplier_name) {
        this.id = id;
        this.product_name = product_name;
        this.expiration_date = expiration_date;
        this.date_of_receipt = date_of_receipt;
        this.supplier_name = supplier_name;
    }

    public Product(String product_name,double price, String expiration_date, String date_of_receipt, String supplier_name) {
        this.product_name = product_name;
        this.price = price;
        this.expiration_date = expiration_date;
        this.date_of_receipt = date_of_receipt;
        this.supplier_name = supplier_name;
    }

    public Long getId() {
        return id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public double getPrice() {
        return price;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public String getDate_of_receipt() {
        return date_of_receipt;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public void setDate_of_receipt(String date_of_receipt) {
        this.date_of_receipt = date_of_receipt;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) && Objects.equals(product_name, product.product_name) && Objects.equals(expiration_date, product.expiration_date) && Objects.equals(date_of_receipt, product.date_of_receipt) && Objects.equals(supplier_name, product.supplier_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, product_name, expiration_date, date_of_receipt, supplier_name);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", product_name='" + product_name + '\'' +
                ", price='" + price + '\'' +
                ", expiration_date='" + expiration_date + '\'' +
                ", date_of_receipt='" + date_of_receipt + '\'' +
                ", supplier_name='" + supplier_name + '\'' +
                '}';
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
