package Util.Factories;

import Util.DataSourceImple;
import repositories.ProductRepository;
import repositories.ProductRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ProductRepositoriesFactory {

    public static ProductRepository onJdbc() {
        Properties properties;
        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new DataSourceImple(properties);
        return new ProductRepositoryJdbcImpl(dataSource);
    }
}
