package Validators;

import Models.Product;
import Validators.exception.ProductNotFoundException;

public interface ProductPresentValidator {
    void productValidator (Product product) throws ProductNotFoundException;
}
