import Models.Product;
import Util.DataSourceImple;
import Util.Factories.ProductRepositoriesFactory;
import repositories.ProductRepository;
import repositories.ProductRepositoryJdbcImpl;
import services.ProductServices;
import services.ProductServicesImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws ParseException {

        ProductRepository productRepository = ProductRepositoriesFactory.onJdbc();
        ProductServices services = new ProductServicesImpl(productRepository);

        // services.signUp("Beer",90,"2022-11-20","2021-10-21","Volkovskaya");
        // services.signIn("Milk");
        //  System.out.println(productRepository.findAllByPrice(70.0));
        Product product = productRepository.findById(2L).orElseThrow(IllegalArgumentException::new);
        product.setPrice(55);
        productRepository.update(product);
        //productRepository.delete(product);

    }
}
