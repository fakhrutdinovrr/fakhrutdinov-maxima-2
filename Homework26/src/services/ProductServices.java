package services;

import java.text.ParseException;

public interface ProductServices {
    void signUp (String product_name, Integer price, String expiration_date, String date_of_receipt, String supplier_name) throws ParseException;

    void signIn(String product_name);
}
