import org.w3c.dom.Node;

/**
 * 12.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList <E> {

    private static class Node <V> {
        V value;
        Node <V> next;

        Node(V value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node <E> first;
    // ссылка на последний элемент
    private Node <E> last;

    private int size = 0;

    public void add(E element) {
        // для нового элемента создаем узел
        Node <E> newNode = new Node(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(E element) {
        // для нового элемента создаем узел
        Node <E> newNode = new Node(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public E get(int index) {
        // начинаем с первого элемента
        Node <E> current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        System.err.println("В списке нет такого индекса");
        return null;
    }

    public void removeAt(int index) {
        Node <E> previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node <E> forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void remove(E element) {
        if (element.equals(first.value)) {
            first = first.next;
            size--;
            return;

        }

        if (element.equals(last.value)) {
            Node <E> previousNode = first;
            while (previousNode.next != last) {
                previousNode = previousNode.next;
            }
            last = previousNode;
            last.next = null;
            size--;
            return;
        }

        Node <E> current = first;

        while (current.next != null) {
            if (element.equals(current.next.value)) {
                current.next = current.next.next;
                size--;
                return;
            }
            current = current.next;
        }
        System.err.println("Element not found.");
    }

    public void removeLast(E element) {
        reverse();
        remove(element);
        reverse();
        size--;
    }

    public void removeAll(E element) {
        Node current = first;
        for (int i = 0; i < size - 1; i++) {
            if (element == current.value) {
                remove(element);
            }
            current = current.next;
        }
    }

    public void add(int index, E element) {
        Node <E> current = first;
        Node <E> newNode = new Node(element);
        if (isEmpty()) {
            first = newNode;
            return;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            size++;
            return;
        }

        if (index == size) {
            last.next = newNode;
            last = newNode;
            size++;
            return;
        }

        for (int i = 0; i < index - 1; i++) {
            current = current.next;
        }
        Node <E> temp = current.next;
        current.next = newNode;
        newNode.next = temp;
        size++;


    }

    public void reverse() {
        Node <E> current = first;
        Node <E> reversedPart = null;
        while (current != null) {
            Node <E> next = current.next;
            current.next = reversedPart;
            reversedPart = current;
            current = next;
        }
        first = reversedPart;
    }

    public void print() {
        Node <E> current = first;
        while (current != null) {
            System.out.print(current.value + " ");
            current = current.next;
        }

        System.out.println();
    }
}
