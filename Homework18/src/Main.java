import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void countsWords(String[] arrayWords, String word) {
        Map<String, Integer> counts = new HashMap<>();

        for (int i = 0; i < arrayWords.length; i++) {

            if (counts.containsKey(arrayWords[i])) {
                int lastValue = counts.get(arrayWords[i]);
                lastValue++;

                counts.put(arrayWords[i], lastValue);
            } else {
                counts.put(arrayWords[i], 1);
            }
        }
        if (counts.containsKey(word)) {
            System.out.println(counts.get(word));
        } else {
            System.out.println("В данном тексте нет такого слова!");
        }
    }

    public static void countsWordsWithToLowerCase(String[] arrayWords, String word) {
        Map<String, Integer> counts = new HashMap<>();

        for (int i = 0; i < arrayWords.length; i++) {
            String currentWordToLowerCase = arrayWords[i].toLowerCase();
            if (counts.containsKey(currentWordToLowerCase)) {
                int lastValue = counts.get(currentWordToLowerCase);
                lastValue++;

                counts.put(currentWordToLowerCase, lastValue);
            } else {
                counts.put(currentWordToLowerCase, 1);
            }
        }
        if (counts.containsKey(word)) {
            System.out.println(counts.get(word));
        } else {
            System.out.println("В данном тексте нет такого слова!");
        }

    }

    public static void main(String[] args) {
        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char characters[];

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;
        try {
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                // преобразуем байт в символ
                char character = (char) currentByte;
                // кидаем символ в массив
                characters[position] = character;
                position++;
                // считываем байт заново
                currentByte = inputStream.read();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


        // создаем строку на основе массива
        String text = new String(characters);
//        System.out.println(text);

        String[] words = text.split("\\W+");
//        System.out.println(Arrays.toString(words));


        countsWordsWithToLowerCase(words, "java");

        countsWords(words,"java");

    }
}
