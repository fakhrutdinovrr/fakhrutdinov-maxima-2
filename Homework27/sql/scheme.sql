drop table if exists foodstuffs;

create table foodstuffs

(
    id              serial primary key,
    product_name    varchar(20)  not null,
    price           int default 0 not null check ( price > -1 ),
    expiration_date DATE not null,
    date_of_receipt DATE not null,
    supplier_name   varchar(20)  not null
);

