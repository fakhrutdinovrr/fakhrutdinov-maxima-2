package services;

import Models.Product;
import Validators.exception.ProductNotFoundException;
import repositories.ProductRepository;

import java.text.ParseException;
import java.util.Date;
import java.util.Optional;

public class ProductServicesImpl implements ProductServices {
    ProductRepository productRepository;

    public ProductServicesImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * добавляет  товар
     *
     * @param product_name    название товара
     * @param price           цена товара
     * @param expiration_date срок годности
     * @param date_of_receipt дата изготовления
     * @param supplier_name   поставщик
     */
    @Override
    public void signUp(String product_name, Integer price, String expiration_date, String date_of_receipt, String supplier_name) throws ParseException {
        Product product = Product.builder()
                .product_name(product_name)
                .price(price)
                .expiration_date(expiration_date)
                .date_of_receipt(date_of_receipt)
                .supplier_name(supplier_name)
                .build();
        productRepository.save(product);
    }

    @Override
    public void signIn(String product_name) {
        Optional<Product> product = productRepository.findOneByName(product_name);
        if (product.isPresent()) {
            System.out.println(product.get());
            return;
        }
        throw new ProductNotFoundException();
    }


}
