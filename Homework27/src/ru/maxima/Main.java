import Models.Product;
import Util.Factories.ProductRepositoriesFactory;
import repositories.ProductRepository;
import services.ProductServices;
import services.ProductServicesImpl;

import java.text.ParseException;

public class Main {

    public static void main(String[] args) throws ParseException {

        ProductRepository productRepository = ProductRepositoriesFactory.onJdbc();
        ProductServices services = new ProductServicesImpl(productRepository);

//       services.signUp("CraftBeer",200,"2022-11-20","2021-10-21","BrewDog");
        // services.signIn("Milk");
//         System.out.println(productRepository.findAllByPrice(20.0));
        Product product = productRepository.findById(2L).orElseThrow(IllegalArgumentException::new);
        System.out.println(product);
//        product.setPrice(55);
//        productRepository.update(product);
        //productRepository.delete(product);

    }
}
