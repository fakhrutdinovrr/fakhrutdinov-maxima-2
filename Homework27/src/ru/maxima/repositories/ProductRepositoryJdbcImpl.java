package repositories;

import Models.Product;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ProductRepositoryJdbcImpl implements ProductRepository {

    private final JdbcTemplate jdbcTemplate;

    //language=SQL
    private static final String SQL_DELETE_FROM_FOODSTUFFS_BY_ID = "delete from foodstuffs " +
            " where id = ?";
    //language=SQL
    private static final String SQL_INSERT_INTO_FOODSTUFFS = "insert into foodstuffs(product_name,price,expiration_date,date_of_receipt,supplier_name) " +
            "values (?,?,?,?,?)";
    //language=SQL
    private static final String SQL_SELECT_FIRST_BY_PRODUCT = "select * from foodstuffs " +
            "where product_name = ? limit 1";
    //language=SQL
    private static final String SQL_SELECT_BY_PRICE = "select * from foodstuffs " +
            "where price = ?";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from foodstuffs " +
            "where id = ? limit 1";
    //language=SQL
    private static final String SQL_UPDATE_FOODSTUFFS_BY_ID = "update foodstuffs set product_name = ?, price = ?, expiration_date = ?, date_of_receipt = ?, supplier_name = ? " +
            "where id = ?";

    private static final RowMapper<Product> productRowMapper = (row, rowNum) -> Product.builder()
            .id(row.getLong("id"))
            .product_name(row.getString("product_name"))
            .price(row.getDouble("price"))
            .expiration_date(row.getString("expiration_date"))
            .date_of_receipt(row.getString("date_of_receipt"))
            .supplier_name(row.getString("supplier_name"))
            .build();


    public ProductRepositoryJdbcImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void save(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        Date data_expiration = null;
        Date date_receipt = null;
        try {
            Date date = new SimpleDateFormat("YYYY-MM-DD").parse(product.getExpiration_date());
            data_expiration = new java.sql.Date(date.getTime());
            Date date1 = new SimpleDateFormat("YYYY-MM-DD").parse(product.getDate_of_receipt());
            date_receipt = new java.sql.Date(date1.getTime());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }


        Date finalData_expiration = data_expiration;
        Date finalDate_receipt = date_receipt;
        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_FOODSTUFFS, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, product.getProduct_name());
            statement.setDouble(2, product.getPrice());
            statement.setDate(3, (java.sql.Date) finalData_expiration);
            statement.setDate(4, (java.sql.Date) finalDate_receipt);
            statement.setString(5, product.getSupplier_name());
            return statement;
        }, keyHolder);
        Long generatedId = ((Integer) keyHolder.getKeys().get("id")).longValue();
        product.setId(generatedId);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, productRowMapper, price);
    }

    @Override
    public Optional<Product> findOneByName(String name) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_FIRST_BY_PRODUCT, productRowMapper, name));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        jdbcTemplate.update(SQL_UPDATE_FOODSTUFFS_BY_ID,
                product.getProduct_name(),
                product.getPrice(),
                product.getExpiration_date(),
                product.getDate_of_receipt(),
                product.getSupplier_name(),
                product.getId());
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_FIND_BY_ID, productRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void delete(Product product) {
        jdbcTemplate.queryForObject(SQL_DELETE_FROM_FOODSTUFFS_BY_ID, productRowMapper);
    }
}
