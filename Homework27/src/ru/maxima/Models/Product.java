package Models;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Product {
    private Long id;
    private String product_name;
    private double price;
    private String expiration_date;
    private String date_of_receipt;
    private String supplier_name;
}
