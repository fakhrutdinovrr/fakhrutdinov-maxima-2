/**
 * 30.10.2021
 * 12. Bus
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА у " + name +  "НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void drive() {
        if (this.bus != null) {
            this.bus.setGo();
            this.bus.rollCall();
        } else {
            System.err.println("Водитель не находится в автобусе, без него не поедем :D !");;
        }

    }


}
