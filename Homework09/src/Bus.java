/**
 * 30.10.2021
 * 12. Bus
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Bus {
    private int number;
    private String model;
    private Driver driver;
    private boolean isGo;

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
    }

    public void incomePassenger(Passenger passenger) {
        if (this.isGo != false) {
            System.err.println("Автобус находится движении, посадка запрещена.");
            return;
        }
        // проверяем, не превысили ли мы количество мест?
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void setDriver(Driver driver) {
        if (!isGo) {
            this.driver = driver;
            driver.setBus(this);
        } else {
            System.err.println("Находу нельзя менять водителя");
        }
    }

    public void setGo() {
        if (this.isGo == false) {
            this.isGo = true;
        } else {
            System.err.println("Автобус уже едет!");
            return;
        }
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }

    public void rollCall() {
        if (passengers[0] == null) {
            System.out.println("Автобус едет без пассажиров!");
        } else {
            for (int i = 0; i < passengers.length; i++) {
                System.out.println("Пассажир " + passengers[i].getName() + " находится в автобусе номер: " + number);
            }
        }
    }

}
