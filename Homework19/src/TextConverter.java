import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TextConverter {

    public static void toLowerCaseAll(String sourceFileName, String targetFileName) {
        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream(sourceFileName);
        } catch (
                FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        char[] characters;

        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


        int position = 0;

        try {
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                char character = (char) currentByte;
                if (Character.isLetter(character) || character == 32) {
                    characters[position] = character;
                    position++;
                    currentByte = inputStream.read();
                } else {
                    currentByte = inputStream.read();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String text = new String(characters);
        text.toLowerCase();

        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(targetFileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }


        byte[] bytes = text.toLowerCase().getBytes();

        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }


}
