package ru.maxima.validators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.maxima.validators.exceptions.EmailValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
@Component
public class EmailRegexValidator implements EmailValidator {

    private Pattern regexPattern;

    public EmailRegexValidator(@Value("${email.validator.regex}") String regex) {
        this.regexPattern = Pattern.compile(regex);
    }

    @Override
    public void validate(String email) throws EmailValidationException {
        Matcher matcher = regexPattern.matcher(email);
        if(!matcher.matches()){
            throw new EmailValidationException();
        }
    }
}
