package ru.maxima.validators;

import org.springframework.stereotype.Component;
import ru.maxima.validators.exceptions.EmailValidationException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class EmailFormatValidator implements EmailValidator {
    @Override
    public void validate(String email) throws EmailValidationException {
        if (!email.contains("@")) {
            throw new EmailValidationException();
        }
    }
}
