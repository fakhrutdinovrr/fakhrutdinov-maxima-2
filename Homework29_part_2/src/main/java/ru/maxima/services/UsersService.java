package ru.maxima.services;

import ru.maxima.services.exceptions.AuthenticationException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {

    /**
     * Проверяет, есть ли пользователь в базе данных с такими учетными данными
     * @param email Email пользователя
     * @param password пароль пользователя
     */
    void signIn(String email, String password);

    /**
     * Регистрирует пользователя в системе
     * @param firstName имя пользователя
     * @param lastName фамилия пользователя
     * @param email Email-пользователя
     * @param password Пароль пользователя
     * @throws AuthenticationException в случае, если email/пароль неверные
     */
    void signUp(String firstName, String lastName, String email, String password) throws AuthenticationException;
}
