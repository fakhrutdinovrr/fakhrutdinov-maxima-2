package ru.maxima.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "ru.maxima")
@PropertySource("classpath:application.properties")
public class ApplicationConfiguration {

    @Autowired
    Environment environment;

    @Bean
    public DataSource hikariDataSource(HikariConfig configuration) {
        return new HikariDataSource(configuration);
    }
    @Bean
    public HikariConfig configuration() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty("db.hikari.jdbcUrl"));
        config.setDriverClassName(environment.getProperty("db.hikari.driverClassName"));
        config.setUsername(environment.getProperty("db.hikari.username"));
        config.setPassword(environment.getProperty("db.hikari.password"));
        config.setMaximumPoolSize(environment.getProperty("db.hikari.maximumPoolSize",Integer.class));
        return config;
    }
}
