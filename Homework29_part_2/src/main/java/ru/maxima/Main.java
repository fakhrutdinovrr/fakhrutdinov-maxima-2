package ru.maxima;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.maxima.config.ApplicationConfiguration;
import ru.maxima.services.UsersService;
import ru.maxima.validators.EmailValidator;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        EmailValidator validator =context.getBean(EmailValidator.class);
        validator.validate("mail@gmail.com");

        UsersService usersService = context.getBean(UsersService.class);
        usersService.signUp("ravil","fakhrutdinov","ravil1@gmail.com","passw0rd");
    }
}
