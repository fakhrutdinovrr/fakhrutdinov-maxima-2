package ru.maxima.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.maxima.models.User;

import javax.sql.DataSource;
import java.util.*;

/**
 * 21.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Component
public class UsersRepositoryNamedParameterJdbcTemplateImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_ACCOUNT = "insert into account(first_name, last_name, email, password) " +
            "values (:firstName, :lastName, :email, :password);";

    //language=SQL
    private static final String SQL_SELECT_FIRST_BY_EMAIL = "select * from account " +
            "where email = :email limit 1";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_HAS_LICENSE = "select * from account " +
            "where has_license = :hasLicense";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from account " +
            "where id = :id";

    //language=SQL
    private static final String SQL_UPDATE_ACCOUNT_BY_ID = "update account set first_name = :fistName, last_name = :lastName, " +
            "email = :email, password = :password " +
            "where id = :id";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public UsersRepositoryNamedParameterJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<User> usersRowMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .build();

    @Override
    public void save(User user) {
        // объект, который будет сохранять сгенерированные ключи для данного запроса
        KeyHolder keyHolder = new GeneratedKeyHolder();

        Map<String, Object> params = new HashMap<>();

        params.put("firstName", user.getFirstName());
        params.put("lastName", user.getLastName());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());

        namedParameterJdbcTemplate.update(SQL_INSERT_INTO_ACCOUNT, new MapSqlParameterSource(params), keyHolder, new String[]{"id"});

        Long generatedId = ((Integer) keyHolder.getKeys().get("id")).longValue();

        user.setId(generatedId);
    }

    @Override
    public Optional<User> findOneByEmail(String email) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_FIRST_BY_EMAIL,
                    Collections.singletonMap("email", email),
                    usersRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    usersRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<User> findAllByHasLicense(boolean hasLicense) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BY_HAS_LICENSE,
                Collections.singletonMap("hasLicense", hasLicense),
                usersRowMapper);
    }

    @Override
    public void update(User user) {
        Map<String, Object> params = new HashMap<>();

        params.put("id", user.getId());
        params.put("firstName", user.getFirstName());
        params.put("lastName", user.getLastName());
        params.put("email", user.getEmail());
        params.put("password", user.getPassword());

       namedParameterJdbcTemplate.update(SQL_UPDATE_ACCOUNT_BY_ID, params);
    }
}
