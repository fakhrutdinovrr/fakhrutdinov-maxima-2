-- добавление данных
insert into account(first_name)
values ('Марсель');
insert into account(first_name, last_name, experience)
values ('Сергей', 'Жигиль', 13);
insert into account(first_name, last_name, experience)
values ('Максим', 'Анисимов', 18);
insert into account(first_name, last_name, experience)
values ('Леонид', 'Кляйман', 45);
insert into account(first_name, last_name, experience)
values ('Равиль', 'Фахрутдинов', 10);
insert into account(first_name, last_name)
values ('Никита', 'Югин');
insert into account(first_name, last_name)
values ('Лилия', 'Тронина');
insert into account (first_name, last_name, experience)
values ('Динар', 'Хидиятов', 9);

-- -- удалить строку с id = 1
-- delete
-- from driver
-- where id = 1;

-- обновление строк
update account
set experience = 38
where id = 4;
update account
set last_name = 'Сидиков'
where id = 1;

-- добавить колонку
alter table account
    add has_license boolean default true;

update account
set has_license = false,
    experience  = 1
where id = 6;
update account
set has_license = false
where id = 1;

insert into car (model, color, owner_id)
values ('BMW', 'Black', 1),
       ('Dodge', 'Black', 2),
       ('Audi', 'Red', 3),
       ('Toyota', 'Grey', 4),
       ('Mazda', 'Blue', 5),
       ('Bugatti', 'Blue', null),
       ('Porsche', 'White', 3),
       ('Renault', 'Red', 1);

insert into driver_car(car_id, driver_id, start_date, finish_date)
values (2, 6, '2022-01-01', '2022-01-31'),
       (3, 7, '2021-12-01', '2021-12-31'),
       (3, 5, '2021-11-01', '2021-12-31'),
       (1, 4, '2022-01-01', '2022-06-01'),
       (6, 5, '2022-01-01', '2022-12-31');

-- insert into car(model, color, owner_id) values ('Lada', 'Granta', 15);
