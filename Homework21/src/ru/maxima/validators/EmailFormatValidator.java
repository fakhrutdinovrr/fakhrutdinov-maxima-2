package ru.maxima.validators;

import ru.maxima.repositories.UsersRepository;
import ru.maxima.validators.exceptions.EmailValidationException;
import ru.maxima.validators.exceptions.UnuniqueEmailException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EmailFormatValidator implements EmailValidator {
    UsersRepository usersRepository;

    @Override
    public void validate(String email) throws EmailValidationException {
        if (!email.contains("@")) {
            throw new EmailValidationException();
        }
    }

}
