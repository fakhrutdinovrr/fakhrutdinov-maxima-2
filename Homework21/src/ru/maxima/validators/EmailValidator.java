package ru.maxima.validators;

import ru.maxima.validators.exceptions.EmailValidationException;
import ru.maxima.validators.exceptions.UnuniqueEmailException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface EmailValidator {
    void validate(String email) throws EmailValidationException;

}
