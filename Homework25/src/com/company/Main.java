package com.company;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;

import java.util.Properties;

public class Main {

    //Language=SQL
    private static final String SQL_SELECT_FROM_FOODSTUFFS = "select *" + " from foodstuffs order by id";

    public static void main(String[] args) {
        Properties properties;

        try {
            properties = new Properties();
            properties.load(new FileInputStream("db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_FROM_FOODSTUFFS)){
                while (resultSet.next()) {
                    String product_name = resultSet.getString("product_name");
                    Integer price = resultSet.getInt("price");
                    String expiration_date = resultSet.getString("expiration_date");
                    String date_of_receipt = resultSet.getString("date_of_receipt");
                    String supplier_name = resultSet.getString("supplier_name");


                    System.out.println(product_name + " " + price + " " + expiration_date + " " + date_of_receipt + " " + supplier_name);
                }
            }
        } catch(SQLException e){
            throw new IllegalArgumentException(e);
        }

    }
}
