package ru.maxima.homework;

import ru.maxima.homework.models.Car;
import ru.maxima.homework.repositories.CarRepository;
import ru.maxima.homework.repositories.CarRepositoryImpl;

import java.util.List;

/**
 * 05.02.2022
 * 27. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        CarRepository carRepository = new CarRepositoryImpl("input.txt");
        System.out.println(carRepository.findAll());

        List<Car> byColorOrMileage = carRepository.findByColorOrMileage("Black", 0);

        byColorOrMileage
                .stream()
                .map(Car::getNumber)
                .forEach(System.out::println);

        // Вывести количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter

        carRepository.findCountByRangePrice(700000,900000);

        // Вывести цвет автомобиля с минимальной стоимостью. // min + map

        carRepository.findColourWithMinPrice();

        // Среднюю стоимость Camry

        carRepository.middlePrice("Camry");
    }
}
