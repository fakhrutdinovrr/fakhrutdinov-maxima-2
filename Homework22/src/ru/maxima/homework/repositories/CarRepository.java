package ru.maxima.homework.repositories;

import ru.maxima.homework.models.Car;

import java.util.List;
import java.util.Optional;

/**
 * 05.02.2022
 * 27. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CarRepository {
    List<Car> findAll();

    List<Car> findByColorOrMileage(String color, Integer mileage);

    void findCountByRangePrice(int from, int to);

    void findColourWithMinPrice();

    void middlePrice(String model);
}
