package ru.maxima.homework.repositories;

import ru.maxima.homework.exceptions.InvalidRange;
import ru.maxima.homework.models.Car;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

/**
 * 05.02.2022
 * 27. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CarRepositoryImpl implements CarRepository {

    private String fileName;

    public CarRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> toCarMapper = string -> {
        String[] parsedLine = string.split("\\|");
        return new Car(parsedLine[0], parsedLine[1], parsedLine[2],
                Integer.parseInt(parsedLine[3]), Double.parseDouble(parsedLine[4]));
    };

    @Override
    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Car> findByColorOrMileage(String color, Integer mileage) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(toCarMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage().equals(mileage)).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void findCountByRangePrice(int from, int to) {
        if (from >= 0 && from <= to) {
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
                List<Car> cars = reader.lines().map(toCarMapper)
                        .filter(car -> car.getPrice() >= from && car.getPrice() <= to).collect(Collectors.toList());
                System.out.println("Количество уникальный моделей: " + cars.stream().map(car -> car.getModel()).distinct().count());
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        } else {
            throw new InvalidRange();
        }
    }

    @Override
    public void findColourWithMinPrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println("Цвет машины с минимальной ценой: " + reader.lines().map(toCarMapper).min(Comparator.comparing(Car::getPrice)).get().getColor());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void middlePrice(String model) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            System.out.println(reader.lines().map(toCarMapper).filter(car -> car.getModel().equals(model)).mapToDouble(Car::getPrice).average());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


}
