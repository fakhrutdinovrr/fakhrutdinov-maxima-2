package ru.maxima.homework.models;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 05.02.2022
 * 27. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Car {
    private String number;
    private String model;
    private String color;
    private Integer mileage;
    private Double price;

    public Car(String number, String model, String color, Integer mileage, Double price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("number='" + number + "'")
                .add("model='" + model + "'")
                .add("color='" + color + "'")
                .add("mileage=" + mileage)
                .add("price=" + price)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return number.equals(car.number) && model.equals(car.model) && color.equals(car.color) && mileage.equals(car.mileage) && price.equals(car.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, mileage, price);
    }
}
