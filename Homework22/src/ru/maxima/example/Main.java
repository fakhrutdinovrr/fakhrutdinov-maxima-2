package ru.maxima.example;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Stream<Integer> integerStream = Stream.of(4, 5, 10, 16, -1, 20, 46, 71);

        Predicate<Integer> evenPredicate = new Predicate<Integer>() {
            @Override
            public boolean test(Integer number) {
                return number % 2 == 0;
            }
        };

        Consumer<Integer> printNumberConsumer = new Consumer<Integer>() {
            @Override
            public void accept(Integer number) {
                System.out.println(number);
            }
        };

        Consumer<String> printStringConsumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };

        Function<Integer, String> toStringFunction = new Function<Integer, String>() {
            @Override
            public String apply(Integer number) {
                if (number > 10) {
                    return "Большое число!";
                } else {
                    return "Маленькое число!";
                }
            }
        };

        Stream<Integer> evenIntegerStream = integerStream.filter(evenPredicate);

        Stream<String> linesStream = evenIntegerStream.map(toStringFunction);

        linesStream.forEach(printStringConsumer);

    }
}
