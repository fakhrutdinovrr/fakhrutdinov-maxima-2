/**
 * 11.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MapHashImpl<K, V> implements Map<K, V> {

    private static final int MAP_SIZE = 16;

    private Entry<K, V>[] entries;

    private int count;

    public MapHashImpl() {
        this.entries = new Entry[MAP_SIZE];
        this.count = 0;
    }

    @Override
    public void put(K key, V value) {
        // взяли хеш-код у ключа
        int hash = key.hashCode();
        // посчитали индекс
        int index = hash & (MAP_SIZE - 1);
        // если под этим индексом уже что-то лежит
        if (entries[index] != null) {
            // положим просто в конец этого списка значение
            // берем первый элемент текущей ячейки
            Entry<K, V> current = entries[index];

            // проверяем первый узел
            if (current.hash == hash && current.key.equals(key)) {
                // если ключ уже был - заменяем значение этого ключа
                current.value = value;
                // останавливаем работу метода
                return;
            }
            // проверяем остальные
            while (current.next != null) {
                // на каждом шаге надо проверить, а не попали ли мы на тот же самый ключ, который уже был
                if (current.hash == hash && current.key.equals(key)) {
                    // если ключ уже был - заменяем значение этого ключа
                    current.value = value;
                    // останавливаем работу метода
                    return;
                }
                // идем до конца, пока не дойдем до элемента, который последний в этом списке
                current = current.next;
            }
            // как только дошли до последнего - добавляем после него новый элемент
            current.next = new Entry<>(key, value, hash);
            count++;
        } else {
            // в противном случае положим значение
            entries[index] = new Entry<>(key, value, hash);
            count++;
        }
    }

    @Override
    public V get(K key) {
        // взяли хеш-код у ключа
        int hash = key.hashCode();
        // посчитали индекс
        int index = hash & (MAP_SIZE - 1);
        // если под этим индексом уже что-то лежит
        if (entries[index] != null) {
            // берем первый элемент текущей ячейки
            Entry<K, V> current = entries[index];
            while (current != null) {
                // если нашли совпадающий ключ
                if (current.hash == hash && current.key.equals(key)) {
                    // возвращаем значение
                    return current.value;
                }
                // если текущий узел нас не устроил - идем дальше
                current = current.next;
            }
            // если до этого ни разу не нашли ничего
            return null;
        } else {
            // если под этим индексом ничего нет - то значит в принципе тут ничего нет
            return null;
        }
    }

    private static class Entry<K, V> {
        int hash;
        K key;
        V value;
        Entry<K, V> next;

        Entry(K key, V value, int hash) {
            this.key = key;
            this.value = value;
        }
    }
}
