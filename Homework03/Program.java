import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int number = scanner.nextInt();
		int min = Integer.MAX_VALUE;
		int digitSum = 0;
		int result = 0;

		while (number != -1) { 
			int temp = number;
			while (number != 0) {
				digitSum += number % 10;
				number /= 10;
			}
			if (min > digitSum){
				min = digitSum;
				result = temp;
			}
			digitSum = 0;
			number = scanner.nextInt();						
		}
		System.out.println("Number: " + result + " has minimum sum of number = " + min);
	}
}