import java.util.Scanner;

class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int localMin = 0;
        int number = scanner.nextInt();
        int temp = 0;


        while (number != -1) {
            temp = number;
            number = scanner.nextInt();
            if (number < temp && number != -1) {
                temp = number;
                number = scanner.nextInt();
                if (temp < number && number != -1) {
                    localMin++;
                }
            }
        }
        System.out.println("Number of local minimums = " + localMin);
    }
}