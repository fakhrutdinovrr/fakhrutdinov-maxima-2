package ru.maxima.services;

import ru.maxima.models.User;
import ru.maxima.repositories.UsersRepository;
import ru.maxima.services.exceptions.AuthenticationException;
import ru.maxima.validators.EmailValidator;
import ru.maxima.validators.PasswordValidator;

import java.util.Optional;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final EmailValidator emailValidator;

    private final PasswordValidator passwordValidator;

    public UsersServiceImpl(UsersRepository usersRepository,
                            EmailValidator emailValidator,
                            PasswordValidator passwordValidator) {
        this.usersRepository = usersRepository;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    public void signUp(String firstName, String lastName, String email, String password) {
        // сделали валидацию данных
        emailValidator.validate(email);
        passwordValidator.validate(password);
        // создали модель для хранения
        User user = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .password(password)
                .build();
        // сохранили пользователя
        usersRepository.save(user);
    }

    @Override
    public void signIn(String email, String password) throws AuthenticationException {
        // находим пользователя по его email-у
        Optional<User> userOptional = usersRepository.findOneByEmail(email);
        // если мне пришел конкретный пользователь
        if (userOptional.isPresent()) {
            // смотрим его пароль
            User user = userOptional.get();
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationException();
            }
            // если все ок - просто останавливаем работу процедуры
            return;
        }
        // если пользователь не пришел
        throw new AuthenticationException();
    }
}
