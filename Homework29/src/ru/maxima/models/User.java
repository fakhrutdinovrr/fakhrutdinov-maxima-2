package ru.maxima.models;

import lombok.*;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class User {
    private Long id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
}
