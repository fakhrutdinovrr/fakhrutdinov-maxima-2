import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.maxima.services.UsersService;
import ru.maxima.validators.EmailValidator;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        EmailValidator validator = (EmailValidator) context.getBean("emailValidator");
        validator.validate("mail@gmail.com");

        UsersService usersService = context.getBean(UsersService.class);
        usersService.signUp("ravil","fakhrutdinov","ravil@gmail.com","passw0rd");
    }
}
