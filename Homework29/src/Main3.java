import ru.maxima.validators.EmailRegexValidator;
import ru.maxima.validators.EmailValidator;

public class Main3 {
    public static void main(String[] args) {
        EmailValidator regexValidator = new EmailRegexValidator("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$");
        regexValidator.validate("fakhrutdinovrr@gmail.com");
    }
}
