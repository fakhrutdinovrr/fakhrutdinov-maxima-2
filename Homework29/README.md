# Spring JDBC

* Цель: упростить использование JDBC для работы с базами данных
* Задачи - подключить Spring JDBC - микрофреймворк, который устраняет шаблонный код при работе с JDBC


## Зачем упрощать?

* Очень много шаблонного кода:

  - создать connection, создать statement
  - отправить запрос
  - получить ответ
  - сделать итерацию по ResultSet
  - все закрыть
  - обработать SQL-exceptions

## Нужные библиотеки

```
commons-logging-1.2.jar
lombok-1.18.22.jar
postgresql-42.3.3.jar
spring-beans-5.3.16.jar
spring-core-5.3.16.jar
spring-jdbc-5.3.16.jar
spring-tx-5.3.16.jar
```