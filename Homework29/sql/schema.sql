-- insert into driver(id, first_name, last_name, experience) values (5, 'Леонид', 'Кляйман', 45);
-- select nextval('driver_id_seq');

-- удаление таблицы, если эта таблица есть
drop table if exists car;
drop table if exists driver_car;
drop table if exists account;
-- создание таблицы
create table account
(
--     id int unique not null,
--     id serial unique not null,
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    -- если не задано - 0, не может быть не задано, в диапазоне -1 до 99
    experience int default 0 not null check ( experience > -1 and experience < 99)
);

-- таблица машин со связью многие-к-одному - много машин у одного владельца
create table car
(
    id       serial primary key,
    model    varchar(20),
    color    varchar(20),
    owner_id bigint
--     foreign key (owner_id) references account(id)
);

-- таблица доверенностей со связью многие ко многим много машин может быть дано в пользование многим водителям
create table driver_car
(
    car_id      bigint,
    driver_id   bigint,
    start_date  timestamp,
    finish_date timestamp
);

alter table driver_car
    add foreign key (car_id) references car (id);
alter table driver_car
    add foreign key (driver_id) references account (id);

alter table car
    add foreign key (owner_id) references account (id);

alter table account add email varchar(255) unique;
alter table account add password varchar(255);