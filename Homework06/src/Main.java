public class Main {

    public static void selectionSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (min > array[j]) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
        // System.out.println(Arrays.toString(array));
    }

    public static boolean search(int[] array, int element) {
        selectionSort(array);
        boolean hasElement = false;
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;
        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
                System.out.println("шаг");
            } else if (element > array[middle]) {
                left = middle + 1;
                System.out.println("шаг");
            } else {
                hasElement = true;
                break;
            }
            middle = left + (right - left) / 2;
        }
        return hasElement;

    }

    public static void main(String[] args) {
        int[] massive = {123, 22, -1, 0, 15, 3, 17, 5, 228, 36};

        selectionSort(massive);
        search(massive, 0);

    }
}

