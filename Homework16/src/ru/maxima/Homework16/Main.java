package ru.maxima.Homework16;

import ru.maxima.collections.Map;
import ru.maxima.collections.MapHashImpl;
import ru.maxima.collections.Set;
import ru.maxima.collections.SetHashImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Map<String, Integer> counts = new MapHashImpl<>();
        Scanner scanner = new Scanner(System.in);
        String words[] = scanner.nextLine().split(" ");

        for (int i = 0; i < words.length; i++) {
            if (counts.containsKey(words[i])) {
                int lastCount = counts.get(words[i]);
                lastCount++;
                counts.put(words[i], lastCount);
            } else {
                counts.put(words[i], 1);
            }
        }
    }
}
