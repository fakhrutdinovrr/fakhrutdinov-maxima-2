//package ru.maxima.old;
//
//import ru.maxima.Map;
//import ru.maxima.Set;
//
///**
// * 11.12.2021
// * 20. Map
// *
// * @author Sidikov Marsel (First Software Engineering Platform)
// * @version v1.0
// */
//public class MapArraysImpl<K, V> implements Map<K, V> {
//
//    private static final int MAX_MAP_SIZE = 10;
//
//    private K[] keys;
//    private V[] values;
//
//    private int count;
//
//    public MapArraysImpl() {
//        this.keys = (K[])new Object[MAX_MAP_SIZE];
//        this.values = (V[])new Object[MAX_MAP_SIZE];
//    }
//
//    @Override
//    public void put(K key, V value) {
//        // должны проверитЬ, а нет ли уже такого ключа
//        // если есть - надо заменить у него значение
//
//        for (int i = 0; i < count; i++) {
//            // нашли совпадение
//            if (keys[i].equals(key)) {
//                // заменили значение
//                values[i] = value;
//                // остановили выполнение метода
//                return;
//            }
//        }
//        this.keys[count] = key;
//        this.values[count] = value;
//        count++;
//    }
//
//    @Override
//    public V get(K key) {
//        // хотим по ключу получить значение, например get("Виктор")
//        // пробегаем все ключи
//        for (int i = 0; i < count; i++) {
//            // если на i-ой позиции мы встретили ключ, который совпал с тем, который подали на вход
//            if (keys[i].equals(key)) {
//                // возвращаем соответствующее значение
//                return values[i];
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public boolean containsKey(K key) {
//        return false;
//    }
//
//    @Override
//    public Set<K> keySet() {
//        return null;
//    }
//}
