package ru.maxima.classes;

import ru.maxima.collections.Map;
import ru.maxima.collections.MapHashImpl;

/**
 * 21.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        Human marsel = new Human(1, "Марсель", "Сидиков", 10);
        Human secondMarsel = new Human(1, "Марсель", "Сидиков", 25);

        Human razil = new Human(2, "Разиль", "Миниахметов", 15);
        Human secondRazil = new Human(2, "Разиль", "Миниахметов", 15);

        Human maxim = new Human(3, "Максим", "Поздеев", 30);

        System.out.println(marsel.equals(razil));
        System.out.println(marsel.equals(maxim));
        System.out.println(marsel.equals(secondMarsel));

        System.out.println(marsel.hashCode());
        System.out.println(razil.hashCode());
        System.out.println(maxim.hashCode());

        Map<Human, String> humans = new MapHashImpl<>();
        humans.put(marsel, "Всем привет!");
        humans.put(razil, "Когда идем в кафе?");
        humans.put(maxim, "Где ТЗ?");

        System.out.println(humans.get(marsel));
        System.out.println(humans.get(secondRazil));
        System.out.println(humans.get(maxim));
        System.out.println(humans.get(secondMarsel)); // "Всем привет!"

        System.out.println(razil.hashCode());
        System.out.println(secondRazil.hashCode());

        System.out.println(marsel.hashCode());
        System.out.println(secondMarsel.hashCode());
        int i = 0;
    }
}
