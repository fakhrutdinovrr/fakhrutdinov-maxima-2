package ru.maxima.classes;

import ru.maxima.collections.Map;
import ru.maxima.collections.MapHashImpl;

/**
 * 21.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        Map<Point, String> surface = new MapHashImpl<>();

        Point a = new Point(10, 10);
        Point b = new Point(10, 15);
        Point c = new Point(10, 20);
        Point d = new Point(10, 25);

        System.out.println(a.equals(b));
        System.out.println(b.equals(c));
        System.out.println(c.equals(d));

        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
        System.out.println(c.hashCode());
        System.out.println(d.hashCode());

        surface.put(a, "Марсель");
        surface.put(b, "Разиль");
        surface.put(c, "Айрат");
        surface.put(d, "Максим");

        int i = 0;
    }
}
