package ru.maxima.examples;

import ru.maxima.collections.Set;
import ru.maxima.collections.SetHashImpl;

/**
 * 18.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainSet {
    public static void main(String[] args) {
        Set<String> set = new SetHashImpl<>();
        set.add("Привет!");
        set.add("Как дела?");
        set.add("Привет!");
        set.add("Как дела?");

        System.out.println(set.contains("Привет"));
        System.out.println(set.contains("Как дела?"));
        System.out.println(set.contains("Марсель"));
    }
}
