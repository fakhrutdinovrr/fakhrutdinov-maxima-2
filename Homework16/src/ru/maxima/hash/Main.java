package ru.maxima.hash;

import java.util.ArrayList;

/**
 * 11.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    // самый простой вариант
    public static int hash1(String string) {
        // суммирование кодов всех символов
        char[] characters = string.toCharArray();
        int hash = 0;
        for (int i = 0; i < characters.length; i++) {
            hash += characters[i];
        }
        return hash;
    }

    // учитываем позицию символов
    public static int hash2(String string) {
        // суммирование кодов всех символов
        char[] characters = string.toCharArray();
        int hash = 0;
        for (int i = 0; i < characters.length; i++) {
            hash += characters[i] * i;
        }
        return hash;
    }

    // реализация hashCode в классе String
    public static int hash3(String string) {
        int hash = 0;

        char[] characters = string.toCharArray();
        // пробегаюсь по всем символам
        for (int i = 0; i < characters.length; i++) {
            // и каждый раз добавляю в сумму 31 * предыдущее значение + код символа
            hash = 31 * hash + characters[i];
        }

        return hash;
    }

    // hash3("Somet")
    // 83 111 109 101 116

    // hash = 31 * 0 + 83
    // hash = 31 * (31 * 0 + 83) + 111
    // hash = 31 * (31 * (31 * 0 + 83) + 111) + 109
    // hash = 31 * (31 * (31 * (31 * 0 + 83) + 111) + 109) + 101
    // hash = 31 * (31 * (31 * (31 * (31 * 0 + 83) + 111) + 109) + 101) + 116
    // hash =

    // 31 * (31 * (31 * (31 * (31 * 0 + 83) + 111) + 109) + 101) + 116
    // 31 * (31 * (31 * (31 * 83 + 111) + 109) + 101) + 116
    // 31 * (31 * (31 * 31 * 83 + 31 * 111 + 109) + 101) + 116
    // 31 * (31 * 31 * 31 * 83 + 31 * 31 * 111 + 31 * 109) + 101) + 116
    // 31 * 31 * 31 * 31 * 83 + 31 * 31 * 31 * 111 + 31 * 31 * 109 + 31 * 101 + 116
    // 31^4 * 83 + 31^3 * 111 + 31^2 * 109 + 31^1 * 101 + 31^0 * 116
    //  S   o   m   e   t
    // 83 111 109 101 116
    //  0   1   2   3   4
    // hash = sum[ 31^[n - 1 - i] * code(i)]
    // hash = 31^4 * code(0) + 31^3 * code(1) + ... + ...
    public static void main(String[] args) {

        System.out.println((int)'S');
        System.out.println((int)'o');
        System.out.println((int)'m');
        System.out.println((int)'e');
        System.out.println((int)'t');

        System.out.println(hash3("Марсель"));
        System.out.println(hash3("Виктор"));
        System.out.println(hash3("Айрат"));
        System.out.println(hash3("Разиль"));
        System.out.println(hash3("Ирик"));
    }
}
