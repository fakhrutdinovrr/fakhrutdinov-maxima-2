public class SimpleMath {
    private Logger logger;

    public SimpleMath(Logger logger) {
        this.logger = logger;
    }

    public int div (int a, int b) {
        if (a == 0 || b == 0) {
            logger.err("Деление на ноль. возвращено -1. ");
            return -1;
        } else {
            logger.log("Частное рассчитано. Возвращен результат. ");
            return a / b;
        }
    }
}
