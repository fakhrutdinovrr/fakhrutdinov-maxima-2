import java.time.LocalDateTime;

public class Logger {
    private LocalDateTime creationTime;
    private static Logger instance;
    static {
        instance = new Logger();
    }

    public static Logger getLogger() {
        return instance;
    }

    public void log(String message) {
        System.out.println("INFO [ " + LocalDateTime.now() + "] " + message);
    }

    public void err(String message) {
        System.err.println("ERROR [ " + LocalDateTime.now() + "] " + message);
    }
}
