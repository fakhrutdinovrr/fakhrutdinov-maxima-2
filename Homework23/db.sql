drop table if exists foodstuffs;

create table foodstuffs

(
    id              serial primary key,
    product_name    char(20)  not null,
    price           int default 0 not null check ( price > -1 ),
    expiration_date date not null,
    date_of_receipt date not null,
    supplier_name   char(20)  not null
);


insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Milk',70,'10.02.2022','03.02.2022','Very important caw');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Bread',38,'15.02.2022','07.02.2022','harrys');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Butter',91,'03.07.2022','06.02.2022','Very important caw');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Cheese',180,'22.05.2023','03.02.2022','lamber');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Cookies',101,'14.02.2022','03.02.2021','catberry');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Candy',114,'11.12.2022','03.01.2022','rotFront');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Sugar',63,'22.04.2022','03.02.2020','Very important caw');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Salt',88,'09.09.2026','03.02.2021','salt cave');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Egg',65,'27.02.2022','03.02.2022','Chicken farm ');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Tomato',125,'17.02.2022','01.02.2022','Very important caw');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('Cucumber',110,'10.02.2022','06.02.2022','Very important caw');
insert into foodstuffs(product_name,price, expiration_date, date_of_receipt, supplier_name) values ('
canned food',220,'27.10.2024','03.02.2019','Very important caw');

delete from foodstuffs where id = 1;

select * from foodstuffs;

select product_name from foodstuffs where price > 100;

select product_name from foodstuffs where date_of_receipt < '20.02.2020';

select distinct supplier_name from foodstuffs;