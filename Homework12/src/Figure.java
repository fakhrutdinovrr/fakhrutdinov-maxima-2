/**
 * 20.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public abstract class Figure implements Move {
    private double x;
    private double y;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void move(double newX, double newY) {
        this.x = newX;
        this.y = newY;
    }

    public abstract double getPerimeter();
    public abstract double getArea();
}
