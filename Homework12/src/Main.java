/**
 * 20.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(10, 15, 5, 7);
        Circle circle = new Circle(5, 6, 10);
        Rectangle rectangle = new Rectangle(1, 1, 56, 10);
        Square square = new Square(10, 15, 6);
        Move [] move = {ellipse,circle,rectangle,square};



        Figure[] figures = {ellipse, circle, rectangle, square};

//        Figure figure = new Figure(10, 15);

        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i].getPerimeter());
        }


        for (int i = 0; i < move.length; i++) {
            move[i].move(12,12);
        }

    }
}
