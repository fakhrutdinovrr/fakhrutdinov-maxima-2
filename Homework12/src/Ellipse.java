/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Ellipse extends Figure {
    private double firstR;
    private double secondR;

    public Ellipse(double x, double y, double firstR, double secondR) {
        super(x, y);
        this.firstR = firstR;
        this.secondR = secondR;
    }

    @Override
    public double getPerimeter() {
        return 4 * (Math.PI * firstR * secondR + Math.pow((firstR - secondR), 2)) / (firstR + secondR);
    }

    @Override
    public double getArea() {
        return 0;
    }
}
