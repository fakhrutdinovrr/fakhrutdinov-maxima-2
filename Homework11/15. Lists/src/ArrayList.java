/**
 * 09.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// список на основе массива
public class ArrayList {
    private final static int DEFAULT_SIZE = 10;

    // хранилище элементов
    private int[] elements;
    // количество фактических элементов в списке
    private int size;

    public ArrayList() {
        // при создании списка я создал внутри массив на 10 элементов
        this.elements = new int[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     *
     * @param element добавляемый элемент
     */
    public void add(int element) {
        // если у меня переполнен массив
        if (isFullArray()) {
            resize();
        }

        this.elements[size++] = element;
    }

    public void addToBegin(int element) {
        ensureSize();

        for (int i = size; i >= 1; i--) {
            this.elements[i] = this.elements[i - 1];
        }

        this.elements[0] = element;
        size++;
    }

    private void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        int[] newArray = new int[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый массив
        // старый массив будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * Получение элемента по индексу
     *
     * @param index индекс элемента
     * @return элемент, который был добавлен в список под номером index, если такого индекса нет - ошибка
     */
    public int get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("В списке нет такого индекса");
            return -1;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Удаляет все элементы из списка
     */
    public void clear() {
        size = 0;
    }

    /**
     * Возвращает количество элементов списка
     *
     * @return размер списка
     */
    public int size() {
        return size;
    }

    public int[] getElements() {
        return elements;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на одну позицию влево
     * <p>
     * 0 -> [14] 1-> [71] 2-> [82] 3-> [25], size = 4
     * <p>
     * removeAt(1)
     * <p>
     * <p>
     * [14] [82] [25] | [25], size = 3
     *
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        if (index >= 0 && index <= size) {
            int[] newArray = new int[elements.length - 1];
            for (int i = 0; i < index; i++) {
                newArray[i] = this.elements[i];
            }
            for (int i = index; i < newArray.length; i++) {
                newArray[i] = this.elements[i + 1];
            }
            this.elements = newArray;
            this.size--;
        } else {
            System.err.println("Index out of range.");
        }
    }

    /**
     * Удаляет первое вхождение элемента в список
     * <p>
     * 34, 56, 78, 56, 92, 11
     * <p>
     * remove(56)
     * <p>
     * 34, 78, 56, 92, 11
     *
     * @param element
     */
    public void remove(int element) {
        for (int i = 0; i < this.elements.length; i++) {
            if (elements[i] == element) {
                removeAt(i);
                return;
            }
        }
        System.err.println("Element not found.");
    }

    /**
     * Удаляет последнее вхождение элемента в список
     * <p>
     * 34, 56, 78, 56, 92, 11
     * <p>
     * removeLast(56)
     * <p>
     * 34, 56, 78, 92, 11
     *
     * @param element
     */
    public void removeLast(int element) {
        revers(this.elements);
        remove(element);
        revers(this.elements);
    }

    private void revers(int[] array) {
        int[] newArray = new int[this.elements.length];
        for (int i = 0; i < this.elements.length; i++) {
            newArray[i] = this.elements[this.elements.length - 1 - i];
        }
        this.elements = newArray;
    }

    public void removeAll(int element) {
        for (int i = 0; i < this.elements.length; i++) {
            if (element == this.elements[i]) {
                remove(element);
            }
        }
    }

    /**
     * Вставляет элемент в заданный индекс (проверяет условие, index < size)
     * Элемент, который стоял под индексом index сдвигается вправо (как и все остальные элементы)
     * 34, 56, 78, 56, 92, 11
     * <p>
     * add(2, 100)
     * <p>
     * 34, 56, 100, 78, 56, 92, 11
     *
     * @param index   куда вставляем элемент
     * @param element элемент, который будем вставлять
     */
    public void add(int index, int element) {
        int[] newArray = new int[this.elements.length + 1];
        for (int i = 0; i < this.elements.length; i++) {
            if (i == index) {
                continue;
            }
            newArray[i] = this.elements[i];
        }
        newArray[index] = element;
        this.elements = newArray;
    }
}
