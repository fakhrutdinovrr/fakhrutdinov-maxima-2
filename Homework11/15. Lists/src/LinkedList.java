import org.w3c.dom.Node;

/**
 * 12.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node first;
    // ссылка на последний элемент
    private Node last;

    private int size = 0;

    public void add(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public int get(int index) {
        // начинаем с первого элемента
        Node current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        System.err.println("В списке нет такого индекса");
        return -1;
    }

    public void removeAt(int index) {
        Node previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void remove(int element) {
        if (element == first.value) {
            first = first.next;
            size--;
            return;

        }

        if (element == last.value) {
            Node previousNode = first;
            while (previousNode.next != last) {
                previousNode = previousNode.next;
            }
            last = previousNode;
            last.next = null;
            size--;
            return;
        }

        Node current = first;

        while (current.next != null) {
            if (element == current.next.value) {
                current.next = current.next.next;
                size--;
                return;
            }
            current = current.next;
        }
        System.err.println("Element not found.");
    }

    public void removeLast(int element) {
        reverse();
        remove(element);
        reverse();
        size--;
    }

    public void removeAll(int element) {
        Node current = first;
        for (int i = 0; i < size - 1; i++) {
            if (element == current.value) {
                remove(element);
            }
            current = current.next;
        }
    }

    public void add(int index, int element) {
        Node current = first;
        Node newNode = new Node(element);
        if (isEmpty()) {
            first = newNode;
            return;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            size++;
            return;
        }

        if (index == size) {
            last.next = newNode;
            last = newNode;
            size++;
            return;
        }

        for (int i = 0; i < index - 1; i++) {
            current = current.next;
        }
        Node temp = current.next;
        current.next = newNode;
        newNode.next = temp;
        size++;


    }

    public void reverse() {
        Node current = first;
        Node reversedPart = null;
        while (current != null) {
            Node next = current.next;
            current.next = reversedPart;
            reversedPart = current;
            current = next;
        }
        first = reversedPart;
    }

    public void print() {
        Node current = first;
        while (current != null) {
            System.out.print(current.value + " ");
            current = current.next;
        }

        System.out.println();
    }
}
